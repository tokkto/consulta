package pe.gob.trabajo.repository;

import pe.gob.trabajo.domain.Empleador;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import java.util.List;


/**
 * Spring Data JPA repository for the Empleador entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EmpleadorRepository extends JpaRepository<Empleador, Long> {

    @Query("select empleador from Empleador empleador where empleador.nFlgactivo = true")
    List<Empleador> findAll_Activos();

//     @Query("select new map(" + 
//                     " empleador.id as Empleador_id " +
//                     " , empleador.tippersona.vDestipper as EmpleadorTippersona_vDestipper " + 
//                     " , empleador.pernatural.tipdocident.vDescorta as  EmpleadorPernaturalTipdocident_vDescorta " +
//                     " , empleador.pernatural.vNumdoc as EmpleadorPernatural_vNumdoc " +
//                     " , (empleador.pernatural.vNombres || ' ' || empleador.pernatural.vApepat || ' ' || empleador.pernatural.vApemat) as NombEmpleador " +
//                     " , empleador.pernatural.vTelefono as EmpleadorPernatural_vTelefono " +
//                     ") " +
    @Query("select new map( empleador as empleador,(empleador.pernatural.vNombres || ' ' || empleador.pernatural.vApepat || ' ' || empleador.pernatural.vApemat) as NombEmpleador) " +
            " from Empleador empleador " + 
            " where empleador.pernatural.tipdocident.id=?1 and  empleador.pernatural.vNumdoc=?2 and empleador.nFlgactivo = true")
    List<Empleador> findConsultaEmpleadorPerNaturalByIdentDoc(Long id_tdoc,String ndoc);

//     @Query("select new map(" + 
//                     " empleador.id as Empleador_id " +
//                     " , empleador.tippersona.vDestipper as EmpleadorTippersona_vDestipper " + 
//                     " , empleador.perjuridica.tipdocident.vDescorta as  EmpleadorPerjuridicaTipdocident_vDescorta " +
//                     " , empleador.perjuridica.vNumdoc as EmpleadorPerjuridica_vNumdoc " +
//                     " , empleador.perjuridica.vRazsocial as NombEmpleador " +
//                     " , empleador.perjuridica.vTelefono as EmpleadorPerjuridica_vTelefono " +
//                     ") " + 
    @Query("select new map( empleador as empleador, empleador.perjuridica.vRazsocial as NombEmpleador) " +
            " from Empleador empleador " + 
            " where empleador.perjuridica.tipdocident.id=?1 and  empleador.perjuridica.vNumdoc=?2 and empleador.nFlgactivo = true")
    List<Empleador> findConsultaEmpleadorPerJuridByIdentDoc(Long id_tdoc,String ndoc);

    // @Query("select new map(" + 
    //                 " empleador.id as Empleador_id " +
    //                 " , empleador.tippersona.vDestipper as EmpleadorTippersona_vDestipper " + 
    //                 " , empleador.perjuridica.tipdocident.vDescorta as  EmpleadorPerjuridicaTipdocident_vDescorta " +
    //                 " , empleador.perjuridica.vNumdoc as EmpleadorPerjuridica_vNumdoc " +
    //                 " , empleador.perjuridica.vRazsocial as NombEmpleador " +
    //                 " , empleador.perjuridica.vTelefono as EmpleadorPerjuridica_vTelefono " +
    //                 ") " + 
    @Query("select new map( empleador as empleador, empleador.perjuridica.vRazsocial as NombEmpleador) " +
            " from Empleador empleador " + 
            " where empleador.perjuridica.vRazsocial like %?1% and empleador.nFlgactivo = true")
    List<Empleador> findConsultaEmpleadordByRazsocial(String razsoc);

    @Query("select empleador from Empleador empleador where empleador.pernatural.tipdocident.id=?1 and  empleador.pernatural.vNumdoc=?2 and empleador.nFlgactivo = true")
    Empleador findEmpleadorPerNaturalByIdentDoc(Long id_tdoc,String ndoc);

    @Query("select empleador from Empleador empleador where empleador.perjuridica.tipdocident.id=?1 and  empleador.perjuridica.vNumdoc=?2 and empleador.nFlgactivo = true")
    Empleador findEmpleadorPerJuridByIdentDoc(Long id_tdoc,String ndoc);

    @Query("select empleador from Empleador empleador where empleador.perjuridica.vRazsocial like %?1% and empleador.nFlgactivo = true")
    List<Empleador> findEmpleadordByRazsocial(String razsoc);
}
