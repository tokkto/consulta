package pe.gob.trabajo.repository;

import pe.gob.trabajo.domain.Pasegl;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import java.util.List;


/**
 * Spring Data JPA repository for the Pasegl entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PaseglRepository extends JpaRepository<Pasegl, Long> {

    // Lista todos los pases activos del sistema
    @Query("select pasegl from Pasegl pasegl where pasegl.nFlgactivo = true")
    List<Pasegl> findAll_Activos();

    // Lista todos los pases Generados para un Trabajador
    @Query("select pasegl " + 
            "from Pasegl pasegl where pasegl.atencion.trabajador.id=?1 or pasegl.atencion.datlab.trabajador.id=?1 and pasegl.nFlgactivo=true order by pasegl.tFecreg desc")
    List<Pasegl> findPasegl_General_By_IdTrabajador(Long id_trab);

    // Lista todos los pases para un Trabajador, con atencion en una Oficina segun un estado del pase
    @Query("select pasegl " + 
            "from Pasegl pasegl where pasegl.estadopase.id=?3 and pasegl.atencion.trabajador.id=?1 or pasegl.atencion.datlab.trabajador.id=?1 and pasegl.oficina.id=?2 and pasegl.nFlgactivo=true order by pasegl.tFecreg desc")
    List<Pasegl> findPasegl_Pendientes_By_IdTrabajador_IdOficina(Long id_trab,Long  id_ofic,String vEstado);

    // Lista todos los pases generados para un Empleador
    @Query("select new map(pasegl.id as id_pasegl " +
                    " , pasegl.atencion.id as id_atencion " + 
                    " , pasegl.tFecreg as fecha " +
                    " , pasegl.atencion.vNumticket as vNumticket " +
                    " , pasegl.atencion.oficina.vDesofic as oficinaOrigen " +
                    " , pasegl.oficina.vDesofic as oficinaDestino " +
                    " , (select repre1.pernatural.tipdocident.vDescorta " + 
                        " from Trabajador repre1 where repre1.id=pasegl.atencion.nCodtrepre) as tipdoc " + 
                    " , (select repre2.pernatural.vNumdoc " + 
                        " from Trabajador repre2 where repre2.id=pasegl.atencion.nCodtrepre) as vNumdoc " + 
                    " , (select repre3.pernatural.vNombres || ' ' || repre3.pernatural.vApepat || ' ' || repre3.pernatural.vApemat " + 
                        " from Trabajador repre3 where repre3.id=pasegl.atencion.nCodtrepre) as nomrepre " + 
                    " , pasegl.nUsuareg as nUsuareg " +
                    " , pasegl.nSedereg as nSedereg " +
                    " ) " +
            " from Pasegl pasegl " + 
            " where pasegl.atencion.empleador.id=?1 and pasegl.nFlgactivo=true order by pasegl.tFecreg desc ")
    List<Pasegl> findLista_General_PaseglByIdEmpleador(Long id_empl);

    // Lista todos los pases de un empleador por medio del Vinculo Laboral
    @Query("select new map(pasegl.id as id_pasegl " +
                    " , pasegl.atencion.id as id_atencion " + 
                    " , pasegl.tFecreg as fecha " +
                    " , pasegl.atencion.vNumticket as vNumticket " +
                    " , pasegl.atencion.oficina.vDesofic as oficinaOrigen " +
                    " , pasegl.oficina.vDesofic as oficinaDestino " +
                    " , (select repre1.pernatural.tipdocident.vDescorta " + 
                        " from Trabajador repre1 where repre1.id=pasegl.atencion.nCodtrepre) as tipdoc " + 
                    " , (select repre2.pernatural.vNumdoc " + 
                        " from Trabajador repre2 where repre2.id=pasegl.atencion.nCodtrepre) as vNumdoc " + 
                    " , (select repre3.pernatural.vNombres || ' ' || repre3.pernatural.vApepat || ' ' || repre3.pernatural.vApemat " + 
                        " from Trabajador repre3 where repre3.id=pasegl.atencion.nCodtrepre) as nomrepre " + 
                    " , pasegl.nUsuareg as nUsuareg " +
                    " , pasegl.nSedereg as nSedereg " +
                    " ) " +
            " from Pasegl pasegl " + 
            " where pasegl.atencion.datlab.empleador.id=?1 and pasegl.nFlgactivo=true order by pasegl.tFecreg desc ")
    List<Pasegl> findLista_General_PaseglxVinculoByIdEmpleador(Long id_empl);

    // Lista todos los pases para un Empleador, con atencion en una Oficina segun un estado del pase
    @Query("select new map(pasegl.id as id_pasegl " +
                    " , pasegl.atencion.id as id_atencion " + 
                    " , pasegl.tFecreg as fecha " +
                    " , pasegl.atencion.vNumticket as vNumticket " +
                    " , pasegl.atencion.oficina.vDesofic as oficinaOrigen " +
                    " , pasegl.oficina.vDesofic as oficinaDestino " +
                    " , (select repre1.pernatural.tipdocident.vDescorta " + 
                        " from Trabajador repre1 where repre1.id=pasegl.atencion.nCodtrepre) as tipdoc " + 
                    " , (select repre2.pernatural.vNumdoc " + 
                        " from Trabajador repre2 where repre2.id=pasegl.atencion.nCodtrepre) as vNumdoc " + 
                    " , (select repre3.pernatural.vNombres || ' ' || repre3.pernatural.vApepat || ' ' || repre3.pernatural.vApemat " + 
                        " from Trabajador repre3 where repre3.id=pasegl.atencion.nCodtrepre) as nomrepre " + 
                    " , pasegl.nUsuareg as nUsuareg " +
                    " , pasegl.nSedereg as nSedereg " +
                    " ) " +
            " from Pasegl pasegl " + 
            " where pasegl.estadopase.id=?3 and pasegl.atencion.empleador.id=?1 and pasegl.oficina.id=?2 and pasegl.nFlgactivo=true order by pasegl.tFecreg desc ")
    List<Pasegl> findListaPaseglByIdEmpleador_IdOficina_estadoPase(Long id_empl,Long  id_ofic,String vEstado);

    // Lista todos los pases, de todas las oficinas con cualquier estado de pase
    @Query("select pasegl from Pasegl pasegl where pasegl.id=?1 and pasegl.nFlgactivo = true")
    List<Pasegl> findPaseglById(Long id); 
}
